pragma solidity ^0.4.19;

contract MockClub {
  struct Member {
    bytes20 username;
    uint64 karma; 
    uint16 canWithdrawPeriod;
    uint16 birthPeriod;
  }

  // Manage members.
  mapping(address => Member) public members;

  function MockClub () public {
    members[msg.sender].birthPeriod = 1;
  }

  function() payable public {}
}
