package contract_test

import (
	"button/pkg/contract"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
)

func TestClose(t *testing.T) {
	sim, ta, buttonAddr := setupButton(buttonCfg{
		countdown: 2,
	})

	button, _ := contract.NewButton(buttonAddr, sim)
	b := contract.ButtonRaw{
		Contract: button,
	}

	// Contract has 3000.
	if _, err := b.Transfer(&bind.TransactOpts{
		From:   ta[0].Auth.From,
		Signer: ta[0].Auth.Signer,
		Value:  big.NewInt(3000),
	}); err != nil {
		t.Fatal(err)
	}
	sim.Commit()

	// press button
	if !sim.succeed(ta[0].Press()) {
		t.Fatalf("seed press failed")
	}

	for i := 0; i < 9; i++ {
		sim.Commit()
	}

	if sim.succeed(ta[0].Close()) {
		t.Fatal("last presser can't close before or on endblock")
	}

	sim.Commit()

	if sim.succeed(ta[1].Close()) {
		t.Fatal("non-last-presser can't close")
	}

	// Successful close
	// - ensure event was sent
	// - ensure balance transfer
	eventFilter, _ := contract.NewButtonFilterer(buttonAddr, sim)
	winEvents := make(chan *contract.ButtonWinner, 1)
	winSub, _ := eventFilter.WatchWinner(nil, winEvents)
	defer winSub.Unsubscribe()

	bal, _ := sim.BalanceAt(nil, ta[0].Addr, nil)
	if !sim.succeed(ta[0].Close()) {
		t.Fatal("last presser can't close")
	}

	if winEvent := <-winEvents; winEvent.Winner.String() != ta[0].Addr.String() || winEvent.Winnings.Uint64() != 3000 {
		t.Fatalf("win event invalid: %+v", winEvent)
	}

	if afterBal, _ := sim.BalanceAt(nil, ta[0].Addr, nil); afterBal.Uint64() != bal.Uint64()+3000 {
		t.Fatal("winnings not transferred properly")
	}
}

func TestBenefactorSignup(t *testing.T) {
	// call newPresserFee on stranger (expect non-zero)
	// call newPresserFee on user (should be zero)

	sim, ta, _ := setupButton(buttonCfg{
		signupFee: 1000, // 10%
		pressFee:  100,
		countdown: 10,
	})

	ta[0].TransactOpts.Value = new(big.Int).SetUint64(100)
	if !sim.succeed(ta[0].Press()) {
		t.Fatalf("seed press failed")
	}

	if strangerFee, _ := ta[1].NewPresserFee(); strangerFee.Uint64() == 0 {
		t.Fatal("expect non-zero new presser fee for stranger")
	}

	if memberFee, _ := ta[14].NewPresserFee(); memberFee.Uint64() != 0 {
		t.Fatalf("expect zero new presser fee for benefactor. received: %d", memberFee.Uint64())
	}
}

func TestBenefactorPayment(t *testing.T) {
	// press button with stranger
	// ensure benefactor balance increases

	sim, ta, buttonAddr := setupButton(buttonCfg{
		signupFee: 1000, // 10%
		pressFee:  100,
		countdown: 10,
	})

	button, _ := contract.NewButton(buttonAddr, sim)
	b := contract.ButtonRaw{
		Contract: button,
	}

	// Contract has 3000.
	if _, err := b.Transfer(&bind.TransactOpts{
		From:   ta[0].Auth.From,
		Signer: ta[0].Auth.Signer,
		Value:  big.NewInt(3000),
	}); err != nil {
		t.Fatal(err)
	}
	sim.Commit()

	bal, _ := sim.BalanceAt(nil, ta[0].Addr, nil)

	// 420 = 300 signup fee + 100 press fee + 20 extra
	// 300 signup = 3000 balance * 10% signupFee
	ta[0].TransactOpts.Value = new(big.Int).SetUint64(420)
	if !sim.succeed(ta[0].Press()) {
		t.Fatalf("seed press failed")
	}

	// Should be only 400 less
	if afterBal, _ := sim.BalanceAt(nil, ta[0].Addr, nil); bal.Uint64()-afterBal.Uint64() != 400 {
		t.Fatal("should only transfer 400 wei")
	}

	clubAddr, _ := ta[0].Club()
	if clubBal, _ := sim.BalanceAt(nil, clubAddr, nil); clubBal.Uint64() != 300 {
		t.Fatal("club balance should be 300")
	}
}

func TestPressCountdown(t *testing.T) {
	t.Log("can't press button after end block")
	{
		testCfg := buttonCfg{countdown: 5}
		sim, ta, _ := setupButton(testCfg)

		if !sim.succeed(ta[0].Press()) {
			t.Fatal("can't press button")
		}

		// min countdown = 10
		for i := 0; i < 10; i++ {
			sim.Commit()
		}

		if sim.succeed(ta[0].Press()) {
			t.Fatal("can't press button after ending")
		}
	}

	t.Log("ensure countdown is set properly")
	{
		// ensure that lastPresser and endBlock are always set correctly
		// ensure that event is always emitted.
		testCfg := buttonCfg{countdown: 150, countdownDecrement: 2}
		sim, ta, buttonAddr := setupButton(testCfg)

		eventFilter, _ := contract.NewButtonFilterer(buttonAddr, sim)
		pressEvents := make(chan *contract.ButtonPressed, 1)
		pressSub, _ := eventFilter.WatchPressed(nil, pressEvents, nil)
		defer pressSub.Unsubscribe()

		for i := 0; i < 100; i++ {
			if !sim.succeed(ta[0].Press()) {
				t.Fatal("can't press button")
			}

			if err := checkAddress(ta[0].LastPresser())(ta[0].Addr); err != nil {
				t.Fatal(err)
			}

			cb := sim.Chain().CurrentBlock().NumberU64()
			countdown := int(testCfg.countdown) - (i * int(testCfg.countdownDecrement))
			if countdown < 10 {
				countdown = 10
			}
			expectedEnd := cb + uint64(countdown)
			if eb, _ := ta[0].EndBlock(); eb != expectedEnd {
				t.Fatalf("end block. expected: %d, received: %d, current block: %d, countdown: %d", expectedEnd, eb, cb, countdown)
			}

			pressEvent := <-pressEvents
			if pressEvent.Presser.String() != ta[0].Addr.String() || pressEvent.EndBlock.Uint64() != expectedEnd {
				t.Fatalf("invalid event: %+v", pressEvent)
			}
			t.Logf("press: %d, countdown: %d", i+1, expectedEnd-cb)
		}
	}

}

func TestPressCooloff(t *testing.T) {
	// set cooloff
	testCfg := buttonCfg{countdown: 100, cooloffIncrement: 2}
	sim, ta, _ := setupButton(testCfg)

	if !sim.succeed(ta[0].Press()) {
		t.Fatal("can't press button")
	}

	// cooloff increment = 2
	// press | blocks until next successful press
	//     1 | 2
	//     2 | 4
	//     3 | 6
	//     4 | 8
	//     5 | 10
	for i := 1; i <= 5; i++ {
		// fastfoward to cooloff - 1
		for j := 0; j < (i*int(testCfg.cooloffIncrement))-1; j++ {
			sim.Commit()
		}

		if sim.succeed(ta[0].Press()) {
			t.Fatal("shouldn't be able to press button in cooloff period")
		}

		sim.Commit()

		if !sim.succeed(ta[0].Press()) {
			t.Fatal("can press after cooloff ends")
		}
	}
}

func TestPressPayment(t *testing.T) {
	// new user, no signup fee, payment < press, fails
	// new user, no signup fee, payment = press, returns no change
	// new user, no signup fee, payment > press, returns change
	// new user, yes signup fee, payment < press+fee, fails
	// new user, yes signup fee, payment = press+fee, returns no change
	// new user, yes signup fee, payment > press+fee, returns change
	// existing user, yes signup fee, payment < press, fails
	// existing user, yes signup fee, payment = press, returns no change
	// existing user, yes signup fee, payment > press, returns change
	for _, test := range []struct {
		cfg      buttonCfg
		existing bool
		value    *big.Int
		change   *big.Int // if change is nil, txn should fail.
	}{
		{cfg: buttonCfg{countdown: 100, pressFee: 100}, value: big.NewInt(99)},
		{cfg: buttonCfg{countdown: 100, pressFee: 100}, value: big.NewInt(100), change: big.NewInt(0)},
		{cfg: buttonCfg{countdown: 100, pressFee: 100}, value: big.NewInt(200), change: big.NewInt(100)},
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, value: big.NewInt(105)},                         // signupfee = 10
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, value: big.NewInt(110), change: big.NewInt(0)},  // signupfee = 10
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, value: big.NewInt(200), change: big.NewInt(90)}, // signupfee = 10
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, existing: true, value: big.NewInt(99)},
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, existing: true, value: big.NewInt(100), change: big.NewInt(0)},
		{cfg: buttonCfg{countdown: 100, pressFee: 100, signupFee: 1000}, existing: true, value: big.NewInt(200), change: big.NewInt(100)},
	} {
		sim, ta, _ := setupButton(test.cfg)

		if test.existing {
			ta[0].TransactOpts.Value = new(big.Int).SetUint64(test.cfg.pressFee)
			if !sim.succeed(ta[0].Press()) {
				t.Fatalf("seed press failed")
			}
		}

		// seed contract with value (by way of button press)
		if test.cfg.signupFee != 0 {
			ta[1].TransactOpts.Value = new(big.Int).SetUint64(10000000) // sufficient to pay press + signupfee
			if !sim.succeed(ta[1].Press()) {
				t.Fatalf("seed press failed")
			}
		}

		ta[0].TransactOpts.Value = test.value
		if test.change == nil {
			if sim.succeed(ta[0].Press()) {
				t.Fatalf("press should fail: cfg=%+v, val=%v, change=%v", test.cfg, test.value, test.change)
			}
			continue
		}

		bal, _ := sim.BalanceAt(nil, ta[0].Addr, nil)
		if !sim.succeed(ta[0].Press()) {
			t.Fatalf("press shouldn't fail: cfg=%+v, val=%v, change=%v", test.cfg, test.value, test.change)
		}

		bal2, _ := sim.BalanceAt(nil, ta[0].Addr, nil)
		if diff := bal.Sub(bal, bal2); diff.Cmp(new(big.Int).Sub(test.value, test.change)) != 0 {
			t.Fatalf("expected balance difference: %v - %v, actual balance difference: %v", test.value, test.change, diff)
		}
	}
}

func TestNewPresserFee(t *testing.T) {
	sim, ta, _ := setupButton(buttonCfg{
		signupFee: 100,
	})

	br := contract.ButtonRaw{Contract: ta[1].Contract}
	ta[1].TransactOpts.Value = big.NewInt(5000)
	if !sim.succeed(br.Transfer(&ta[1].TransactOpts)) {
		t.Fatal("can't send")
	}

	if f, _ := ta[0].NewPresserFee(); f.Cmp(big.NewInt(50)) != 0 {
		t.Fatalf("newPresserFee. expected: %d, received: %d", 50, f)
	}
}
