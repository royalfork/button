//go:generate abigen --sol Button.sol --pkg contract --out button.go
//go:generate abigen --sol MockClub.sol --pkg contract --out mockclub.go
package contract_test

import (
	"button/pkg/contract"
	"crypto/ecdsa"
	"crypto/rand"
	"flag"
	"fmt"
	"math/big"
	mrand "math/rand"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/accounts/abi/bind/backends"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
)

type TestAccount struct {
	*contract.ButtonSession
	Addr common.Address
	Priv *ecdsa.PrivateKey
	Auth *bind.TransactOpts
}

type TestSim struct {
	*backends.SimulatedBackend
}

var (
	trackGas     bool
	contractABIs = make(map[string]abi.ABI)
)

func init() {
	flag.BoolVar(&trackGas, "trackGas", false, "Log gas usage")
	flag.Parse()

	for contractName, contractABIRaw := range map[string]string{
		"Button": contract.ButtonABI,
	} {
		abi, err := abi.JSON(strings.NewReader(contractABIRaw))
		if err != nil {
			panic(err)
		}
		contractABIs[contractName] = abi
	}
}

func logGas(data []byte, r *types.Receipt) {
	if len(data) < 4 {
		fmt.Printf("%7d | %10s | %16s | %1d\n", r.CumulativeGasUsed, "n/a", "n/a", r.Status)
		return
	}
	for contractName, contractABI := range contractABIs {
		m, _ := contractABI.MethodById(data[:4])
		if m != nil {
			fmt.Printf("%7d | %10s | %16s | %1d\n", r.CumulativeGasUsed, contractName, m.Name, r.Status)
			return
		}
	}
	panic("function not found")
}

func (ts *TestSim) succeed(txn *types.Transaction, err error) bool {
	if err != nil {
		return false
	}
	ts.Commit()
	r, err := ts.TransactionReceipt(nil, txn.Hash())
	if err != nil {
		return false
	}

	if trackGas {
		logGas(txn.Data(), r)
	}

	return r.Status == 1
}

type buttonCfg struct {
	countdown          uint64
	countdownDecrement uint64
	cooloffIncrement   uint64
	pressFee           uint64
	signupFee          uint64
}

func setupButton(cfg buttonCfg) (*TestSim, []TestAccount, common.Address) {
	// create genesis accounts
	var testAccounts []TestAccount
	genesis := make(core.GenesisAlloc)
	for _, pk := range []string{
		"1010101010101010101010101010101010101010101010101010101010101010",
		"1111111111111111111111111111111111111111111111111111111111111111",
		"2222222222222222222222222222222222222222222222222222222222222222",
		"3333333333333333333333333333333333333333333333333333333333333333",
		"4444444444444444444444444444444444444444444444444444444444444444",
		"5555555555555555555555555555555555555555555555555555555555555555",
		"6666666666666666666666666666666666666666666666666666666666666666",
		"7777777777777777777777777777777777777777777777777777777777777777",
		"8888888888888888888888888888888888888888888888888888888888888888",
		"9999999999999999999999999999999999999999999999999999999999999999",
		"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
		"cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc",
		"dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd",
		"eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
	} {
		key, _ := crypto.HexToECDSA(pk)

		genesis[crypto.PubkeyToAddress(key.PublicKey)] = core.GenesisAccount{
			Balance: big.NewInt(1e18), // change to 1 eth?
		}

		// For testing...gas price will be 0 to keep balance inquiries easy.
		t := bind.NewKeyedTransactor(key)
		t.GasPrice = big.NewInt(0)

		testAccounts = append(testAccounts, TestAccount{
			Addr: crypto.PubkeyToAddress(key.PublicKey),
			Priv: key,
			Auth: t,
		})
	}

	sim := backends.NewSimulatedBackend(genesis)

	clubAddr, _, _, err := contract.DeployMockClub(testAccounts[14].Auth, sim)
	if err != nil {
		panic(err)
	}

	// Deploy contract on simulated backend
	buttonAddr, _, button, err := contract.DeployButton(testAccounts[0].Auth, sim, cfg.countdown, cfg.countdownDecrement, cfg.cooloffIncrement, cfg.pressFee, cfg.signupFee, clubAddr)
	if err != nil {
		panic(err)
	}

	sim.Commit()
	// Create contract sessions for test accounts
	for i := 0; i < len(testAccounts); i++ {
		testAccounts[i].ButtonSession = &contract.ButtonSession{
			Contract: button,
			CallOpts: bind.CallOpts{
				Pending: true,
				From:    testAccounts[i].Addr,
			},
			TransactOpts: *testAccounts[i].Auth,
		}
	}

	return &TestSim{sim}, testAccounts, buttonAddr
}

func checkAddress(received common.Address, err error) func(expected common.Address) error {
	return func(expected common.Address) error {
		if expected.String() != received.String() {
			return fmt.Errorf("expected: %s, received: %s", expected.String(), received.String())
		}
		return nil
	}
}

func randomUint() *big.Int {
	buf := make([]byte, 32)
	rand.Read(buf)
	return new(big.Int).SetBytes(buf)
}

func random(ta []TestAccount) TestAccount {
	return ta[mrand.Intn(len(ta))]
}
