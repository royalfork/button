var config = {
  netId: '19',
  blockTime: 12, // average seconds per block
  //contractAddr: "0xAE519FC2Ba8e6fFE6473195c092bF1BAe986ff90", // completed contest
  contractAddr: "0xc43922991e56f5A9f7DEa86A619E79fec931C336",
  clubAddr: "0x501c17b26029f3a129fd9adeb4b642bd35a35248",
  clubSignup: "https://www.royalfork.org",
  pressFee: "1000000000000000", // in wei
  countdown: 1500,
  countdownDecrement: 10,
  cooloffIncrement: 15,
  abi: [{"constant":true,"inputs":[],"name":"newPresserFee","outputs":[{"name":"","type":"uint128"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"endBlock","outputs":[{"name":"","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"close","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"press","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"pressers","outputs":[{"name":"numPresses","type":"uint64"},{"name":"cooloffEnd","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"lastPresser","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_countdown","type":"uint64"},{"name":"_countdownDecrement","type":"uint64"},{"name":"_cooloffIncrement","type":"uint64"},{"name":"_pressFee","type":"uint64"},{"name":"_signupFee","type":"uint64"},{"name":"_benefactor","type":"address"}],"payable":true,"stateMutability":"payable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"presser","type":"address"},{"indexed":false,"name":"endBlock","type":"uint256"}],"name":"Pressed","type":"event"}],
  clubAbi: [{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"members","outputs":[{"name":"username","type":"bytes20"},{"name":"karma","type":"uint64"},{"name":"canWithdrawPeriod","type":"uint16"},{"name":"birthPeriod","type":"uint16"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"}]
};

Vue.filter("toUSD", function(wei) {
  if (wei == null) {
    return null;
  }
  return "$" + (parseFloat(Web3Utils.fromWei(wei, "ether")) * ethPrice).toFixed(2);
});

Vue.filter("toTimeString", function(s) {
  if (s === null) {
    return "";
  }

  if (s < 0) {
    s = 0;
  }

  // https://stackoverflow.com/a/6313008
  var hours   = Math.floor(s / 3600);
  var minutes = Math.floor((s - (hours * 3600)) / 60);
  var seconds = s - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}

  return hours+'h '+minutes+'m '+seconds+"s";
});

Vue.filter("blocksToTime", function(blocks) {
  return blocks * config.blockTime;
})

Vue.filter("toEtherString", function(wei){
  if (wei == null) {
    return null;
  }
  var denom = "ether";
  return Web3Utils.fromWei(wei, denom) + " " + denom;
});

// Style bottowed from: https://codepen.io/anon/pen/WbWYgm
Vue.component('thebutton', {
  template: '#button-template',
  props: ["account", "currentBlock", "endBlock"],
  data: function () {
    return {
      statusText: "Reading metamask address...", // show status while loading.
      pressFee: config.pressFee,
      clubSignup: config.clubSignup,
      numPresses: null,
      cooloffEnd: null,
      signupFee: null,
      username: null,
      pending: false // txn hash of pending button press.
    }
  },
  watch: {
    // When account address changes, get account info.
    account: function(addr) {
      // Account will be null if metamask is locked.
      if (addr == null) {
        this.numPresses = null;
        this.cooloffEnd = null;
        this.statusText = "To press the button, please ensure that your metamask account is unlocked.";
        return;
      }
      console.log("Account:" + addr);
      this.statusText = "Loading account info...";
      this.getPresserInfo();
      this.getUsername();
    },
    // If button has never been pressed, get signup fee.
    numPresses: function(num) {
      if (num == 0 && this.signupFee == null) {
        this.statusText = "Loading prices...";
        this.getSignupFee();

        // signup fee can change based on balance of contract...update it
        this.signupInterval = setInterval(function(){
          this.getSignupFee();
        }.bind(this), 5000);
        return;
      }
      if (num > 0) {
        clearInterval(this.signupInterval);
      }
    }
  },
  computed: {
    // Component must have the following info, before displaying:
    // - numPresses for account
    // - signupFee (if addr has never pressed the button)
    // - currentBlock (if addr has pressed the button)
    loaded: function () {
      if (this.numPresses == null) {
        return false;
      }
      if (this.numPresses == 0 && this.signupFee == null) {
        return false;
      }
      if (this.numPresses > 0 && this.currentBlock == null) {
        return false;
      }
      return true;
    },
    // Includes signupFee, if applicable.
    totalPressFee: function () {
      if (this.numPresses == null) {
        return null;
      }
      if (this.numPresses > 0) {
        return config.pressFee;
      }
      if (this.signupFee == null) {
        return null;
      }
      var pf = Web3Utils.toBN(config.pressFee);
      var sf = Web3Utils.toBN(this.signupFee);
      return pf.add(sf).toString();
    },
    // Given current numPresses, when will presser win the pot?
    winBlock: function () {
      if (this.numPresses == null) {
        return null;
      }
      var wb = config.countdown - (this.numPresses * config.countdownDecrement);
      return (wb > 10 ? wb : 10);
    }
  },
  methods: {
    getSignupFee: function () {
      button.newPresserFee({
        from: this.account
      }, (err, pf) => {
        this.signupFee = pf.toString();
      });
    },
    getUsername: function () {
      var contract = web3.eth.contract(config.clubAbi);
      club = contract.at(config.clubAddr);
      club.members(this.account, (err, m) => {
        this.username = Web3Utils.toAscii(m[0].replace(/(00)*$/, ''))
      });
    },
    getPresserInfo: function () {
      button.pressers(this.account, (err, p) => {
        this.numPresses = p[0].toNumber();
        this.cooloffEnd = p[1].toNumber();
      });
    },
    checkPending: function () {
      console.log("checking pending txn:", this.pending);
      web3js.eth.getTransactionReceipt(this.pending, function(err, r) {
        if (err != null) {
          alert("Error retrieving transaction: " + this.pending + ". Please reload this page.");
          return;
        }

        // Txn hasn't been minded yet.  Retry in 3s.
        if (r == null) {
          setTimeout(function(){
            this.checkPending();
          }.bind(this), 3000)
          return;
        }

        // Pending txn has been mined.
        this.pending = window.localStorage.removeItem("button-pendingtxn"); 

        if (r.status !== "0x1") {
          alert("Press transaction failed. Please try again, or email joe@royalfork.org for more info (include the following txn hash: " + this.pending + ").");
          return;
        }

        this.pending = null;
        this.numPresses++;
        this.cooloffEnd = r.blockNumber + (this.numPresses* config.cooloffIncrement);
      }.bind(this));
    },
    press: function () {
      var tpf = this.totalPressFee;

      // add 20% if first press, so txn doesn't get rejected.
      // this will get returned to user.
      if (this.numPresses == 0) {
        var t = Web3Utils.toBN(tpf);
        tpf = Web3Utils.toBN(tpf).muln(12).divn(10).toString()
      }

      this.pending = "SIGNING";
      web3js.eth.sendTransaction({
        to: config.contractAddr,
        value: tpf,
        data: button.press.getData()
      }, function (error, hash) {
        if (error != null) {
          this.pending = null;
          alert(error.toString().split("\n")[0]);
          return;
        }

        this.pending = hash;
        window.localStorage.setItem("button-pendingtxn", hash);

        this.checkPending();
      }.bind(this));
    }
  },
  created: function() {
    // check local storage
    this.pending = window.localStorage.getItem("button-pendingtxn");
    if (this.pending != null) {
      this.checkPending();
    }
  }
});

Vue.component('countdown', {
  template: '#countdown-template',
  props: ["current-block", "end-block"],
  data: function () {
    return {
      maxBlocks: config.countdown,
      blockTime: config.blockTime,
      countdown: null // approximate time left in seconds.
    }
  },
  watch: {
    currentBlock: function () {
      this.updateCountdown();
    },
    endBlock: function () {
      this.updateCountdown();
    }
  },
  computed: {
    remainingBlocks: function () {
      if (this.endBlock == null || this.currentBlock == null) {
        return null;
      }

      var rb = this.endBlock - this.currentBlock;
      return rb <= 0 ? 0 : rb;
    }
  },
  methods: {
    updateCountdown: function () {
      if (this.remainingBlocks == null) {
        this.countdown = 0;
        this.chart.update(0);
        return;
      }

      this.countdown = this.remainingBlocks * this.blockTime;
      this.chart.update((this.remainingBlocks / this.maxBlocks) * 100);
    }
  },
  mounted: function () {
    // create countdown pie chart
    this.chart = new EasyPieChart(document.querySelector('.chart'), {
      size: 100,
      barColor: "#2160ff",
      trackColor: "#d3dfff",
      scaleColor: false,
      lineWidth: 10
    });

    // update timer
    setInterval(function(){
      if (this.countdown == null) {
        return;
      }
      this.countdown--;
    }.bind(this), 1000);
  }
});

// Globals, set by vue.
var web3js;
var button;
var vm = new Vue({
  el: "#app",
  data: {
    loadingText: null,
    account: null,
    currentBlock: null,
    lastPresser: null,
    endBlock: null,
    balance: null // wei, in string form
  },
  methods: {
    getContractInfo: function () {
      web3.eth.getBalance(config.contractAddr, (err, bal) => {
        this.balance = bal.toString();
      });
      button.lastPresser((err, lp) => {
        this.lastPresser = lp;
      });
      button.endBlock((err, eb) => {
        this.endBlock = parseInt(eb);
      });
    },
    getBlockNumber: function() {
      web3.eth.getBlockNumber((err, bn) => {
        this.currentBlock = parseInt(bn);
      });
    },
    web3js: function () {
      return web3js;
    }
  },
  created: function () {
    if (typeof web3 === 'undefined') {
      this.loadingText = "This app requires MetaMask.";
      return;
    }

    // Best way to get web3 working in vue is to keep it outside vue's data scope.
    // https://stackoverflow.com/questions/43417885/how-to-get-web3js-to-work-inside-a-vuejs-component
    // Must manually force update when web3js is set.
    web3js = new Web3(web3.currentProvider);

    this.loadingText = "Getting network info...";
    web3.version.getNetwork(function(err, netId) {
      if (err != null) {
        alert(err);
        return;
      }

      if (netId != config.netId) {
        this.loadingText = "In MetaMask, please use Main Ethereum Network.";
        return;
      }

      // Metamask and network have been validated.
      this.loadingText = null;

      // Set account.
      setInterval(function() {
        this.account = web3.eth.accounts[0];
      }.bind(this), 1000);

      // Poll for new blocks.
      this.getBlockNumber();
      setInterval(function() {
        this.getBlockNumber();
      }.bind(this), 4000);

      // Set contract
      var contract = web3.eth.contract(config.abi);
      button = contract.at(config.contractAddr);
      console.log("ContractAddr:", config.contractAddr);
      this.getContractInfo();

      // Listen of press and donation events.
      var events = button.allEvents(function(error, log){
        this.getContractInfo();
      }.bind(this));
    }.bind(this));
  }
});
