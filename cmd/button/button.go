package main

import (
	"button/pkg/contract"
	"fmt"
	"math/big"
	"os"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		// TODO figure out better way to get PK
		cli.StringFlag{
			Name:  "priv",
			Value: "1010101010101010101010101010101010101010101010101010101010101010",
			Usage: "hex encoded private key of signer",
		},
		cli.StringFlag{
			Name:  "client",
			Value: "http://127.0.0.1:8545",
			//Value: "https://mainnet.infura.io/JUH9l2Jmfbh7z5WEedqx",
			Usage: "RPC client",
		},
		cli.Uint64Flag{
			Name:  "gp",
			Value: 4000000000,
			Usage: "gas price",
		},
		cli.StringFlag{
			Name:  "addr",
			Usage: "live contract address",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "deploy",
			Usage: "deploy contract",
			Action: func(c *cli.Context) error {
				conn, err := ethclient.Dial(c.GlobalString("client"))
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				auth, err := getAuth(c)
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				// TODO place inputs into deploy flags
				//_countdown uint64, _countdownDecrement uint64, _cooloffIncrement uint64, _pressFee uint64, _signupFee uint64,
				// _benefactor common.Address
				// countdown == 5 hours = 1500 blocks
				// decrement == 10 blocks
				// coolOff = 15 blocks
				// pressFee = .001 eth = 1000000000000000
				// signupFee = 1% = 100 basis points (should be .5%, 50 basis points)
				b := common.HexToAddress("0x501c17b26029f3a129fd9adeb4b642bd35a35248")
				addr, txn, _, err := contract.DeployButton(auth, conn, 1500, 10, 15, 1000000000000000, 100, b)
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				fmt.Println("Txn Hash: ", txn.Hash().String())
				fmt.Println("Address: ", addr.String())
				return nil
			},
		},
		{
			Name:  "press",
			Usage: "presses the button",
			Action: func(c *cli.Context) error {
				s, err := getSession(c)
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				txn, err := s.Press()
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				fmt.Printf("txn = %+v\n", txn)
				return nil
			},
		},
		{
			Name:  "fee",
			Usage: "gets fee",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "caller",
					Usage: "caller address",
				},
			},
			Action: func(c *cli.Context) error {
				s, err := getSession(c)
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				s.CallOpts.From = common.HexToAddress(c.String("caller"))
				f, err := s.NewPresserFee()
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				fmt.Printf("f = %+v\n", f)
				return nil
			},
		},
	}

	app.Run(os.Args)
}

func getAuth(c *cli.Context) (*bind.TransactOpts, error) {
	priv := c.GlobalString("priv")
	key, err := crypto.HexToECDSA(priv)
	if err != nil {
		return nil, err
	}

	gp := c.GlobalUint64("gp")
	auth := bind.NewKeyedTransactor(key)
	auth.GasPrice = big.NewInt(int64(gp))
	return auth, nil
}

func getSession(c *cli.Context) (*contract.ButtonSession, error) {
	conn, err := ethclient.Dial(c.GlobalString("client"))
	if err != nil {
		return nil, err
	}

	auth, err := getAuth(c)
	if err != nil {
		return nil, err
	}

	a := common.HexToAddress(c.GlobalString("addr"))
	if a.String() == new(common.Address).String() {
		return nil, fmt.Errorf("invalid contract addr: %s", a.String())
	}

	button, err := contract.NewButton(a, conn)
	if err != nil {
		return nil, err
	}

	auth.GasLimit = 100000
	auth.Value = big.NewInt(1000000000000000)
	return &contract.ButtonSession{
		Contract: button,
		CallOpts: bind.CallOpts{
			Pending: true,
		},
		TransactOpts: *auth,
	}, nil
}
